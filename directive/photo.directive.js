angular.module('LetLife')
  .directive('photoEditable', ['Storage', '$uibModal', function(Storage, $uibModal, PhotoController){

    function link (scope, elm, attrs, ctrls){
      var ngModel = ctrls[0];
      ngModel.$render = function() {
        imageFilename = ngModel.$viewValue || '1455796977185_56bb0510f70bb0026aec7b4b.png';
        if( imageFilename ){
          var divId =  'photo-'+(((1+Math.random())*0x10000)|0).toString(16);

          return Storage.get( imageFilename ).success(function(dataphoto, status, headers, config) {
            elm.html('<img id="'+divId+'" class="'+attrs.typeimage+' img-rounded " src="'+dataphoto.result.signedUrl+'">');

            if(attrs.onlyview != 'true'){
              elm.bind('click', function() {

                var uib = $uibModal.open({
                  controller : 'PhotoController',
                  controllerAs: 'vm',
                  templateUrl : 'view/shared/PhotoDir/photo.template.html',
                  resolve :  {
                    divId : function(){
                      return divId;
                    }
                  }
                });

                uib.result.then(function(){
                }, function (result) {
                  imageFilename = result;
                  ngModel.$setViewValue(result);
                });
              });
            }

          });
        }
      }
    };

    function getImage(value) {
      if( value ){
        return Storage.get( value ).success(function(dataphoto, status, headers, config) {
          self.elm.html('<img id="ImgAnima" class="img-rounded col-md-12" src="'+dataphoto.result.signedUrl+'">');

          self.elm.bind('click', function() {
            var uib = $uibModal.open({
              controller : 'PhotoController',
              controllerAs: 'vm',
              templateUrl : 'view/shared/PhotoDir/photo.template.html',
            });
            uib.result.then(function (result) {

            }, function (result) {
              self.imageFilename = result;
              self.ngModel.$setViewValue(result);
            });
          });
        });
      }
    };

    return {
      require: ['ngModel'],
      restrict : 'EAC',
      link: link,
      scope: {}
    };

  }]);
